package com.ruben.pojo.po;

import lombok.Data;

import java.util.List;

/**
 * UserInfo
 *
 * @author VampireAchao
 * @since 2022/5/21
 */
@Data
public class UserInfo {


    private static final long serialVersionUID = -7219188882388819210L;
    private Long id;
    private String name;
    private Integer age;
    private String email;
    private List<UserRole> userRoles;


}
