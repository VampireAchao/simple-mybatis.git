package com.ruben.pojo.po;

import lombok.Data;

/**
 * UserRole
 *
 * @author VampireAchao
 * @since 2022/5/23
 */
@Data
public class UserRole {

    private Long id;
    private Long userId;
    private String roleId;
    private RoleInfo role;
}
