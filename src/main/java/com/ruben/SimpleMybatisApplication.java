package com.ruben;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 主启动类
 *
 * @author VampireAchao
 */
@SpringBootApplication
public class SimpleMybatisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleMybatisApplication.class, args);
    }

}
