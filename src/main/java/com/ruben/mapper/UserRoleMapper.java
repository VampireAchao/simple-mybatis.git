package com.ruben.mapper;

import com.ruben.pojo.po.RoleInfo;
import com.ruben.pojo.po.UserRole;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.cursor.Cursor;

import java.util.List;

/**
 * 用户角色Mapper
 *
 * @author VampireAchao
 */
@Mapper
public interface UserRoleMapper {
    /**
     * 流式查询用户角色关联
     *
     * @return 数据流
     */
    Cursor<UserRole> selectListCursor(UserRole userRole);


    @Select("<script>" +
            "select *" +
            "        from user_role\n" +
            "        <where>" +
            "            <if test=\"userId != null\">" +
            "                AND user_id = #{userId}" +
            "            </if>" +
            "            <if test=\"roleId != null\">" +
            "                AND role_id = #{roleId}" +
            "            </if>" +
            "        </where>" +
            "</script>")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "roleId", column = "role_id"),
            @Result(property = "role", javaType = RoleInfo.class, column = "role_id",
                    one = @One(select = "com.ruben.mapper.RoleMapper.getById"))
    })
    List<UserRole> selectList(UserRole userRole);

}
