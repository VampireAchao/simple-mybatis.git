package com.ruben.mapper;

import com.ruben.pojo.po.RoleInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 角色Mapper
 *
 * @author VampireAchao
 */
@Mapper
public interface RoleMapper {
    /**
     * 查询角色
     *
     * @param id id
     * @return 角色
     */
    RoleInfo getById(String id);

}
