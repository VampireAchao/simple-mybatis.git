package com.ruben.mapper;

import com.ruben.pojo.po.UserInfo;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * 用户Mapper
 *
 * @author VampireAchao
 */
@Mapper
public interface UserMapper {
    /**
     * 流式分页查询用户
     *
     * @param rowBounds 分页条件
     * @return 用户流
     */
    Cursor<UserInfo> selectPageCursor(RowBounds rowBounds);

    @Select("SELECT * FROM user_info")
    @Results({
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(column = "age", property = "age"),
            @Result(column = "email", property = "email"),
            @Result(property = "userRoles", javaType = List.class, column = "userRole.userId = id",
                    many = @Many(select = "com.ruben.mapper.UserRoleMapper.selectListCursor"))
    })
    List<UserInfo> selectList(RowBounds rowBounds);


    @MapKey("id")
    @Select("SELECT * FROM user_info")
    Map<Long, UserInfo> selectIdUserMap();

    @Select("SELECT * FROM user_info")
    Map<Long, UserInfo> list();

    @Select("SELECT name FROM user_info")
    List<UserInfo> selectNames();
}
