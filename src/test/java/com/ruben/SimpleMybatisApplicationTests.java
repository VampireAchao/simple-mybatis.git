package com.ruben;

import com.ruben.mapper.UserMapper;
import com.ruben.mapper.UserRoleMapper;
import com.ruben.pojo.po.UserInfo;
import com.ruben.pojo.po.UserRole;
import lombok.SneakyThrows;
import lombok.val;
import org.apache.ibatis.cursor.Cursor;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

@SpringBootTest
class SimpleMybatisApplicationTests {

    @Resource
    private SqlSessionFactory sqlSessionFactory;
    @Resource
    private TransactionTemplate transactionTemplate;
    @Resource
    private UserMapper userMapper;

    @Test
    @SneakyThrows
    void sqlSessionWay() {
        Assertions.assertAll(() -> {
            try (SqlSession session = sqlSessionFactory.openSession(); Cursor<UserInfo> userCursor = session.getMapper(UserMapper.class).selectPageCursor(new RowBounds(0, 5))) {
                StreamSupport.stream(userCursor.spliterator(), true).forEach(System.out::println);
            }
        });
    }

    @Test
    void transactionTemplateWay() {
        Assertions.assertAll(() -> transactionTemplate.executeWithoutResult(transactionStatus -> {
            try (Cursor<UserInfo> userCursor = userMapper.selectPageCursor(new RowBounds(0, 5))) {
                StreamSupport.stream(userCursor.spliterator(), true).forEach(System.out::println);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }));
    }

    @Test
    @Transactional
    void transactionalWay() {
        Assertions.assertAll(() -> {
            try (Cursor<UserInfo> userCursor = userMapper.selectPageCursor(new RowBounds(0, 5))) {
                StreamSupport.stream(userCursor.spliterator(), true).forEach(System.out::println);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }

    @Test
    void testOne(@Autowired UserRoleMapper userRoleMapper) {
        List<UserRole> userRoles = userRoleMapper.selectList(new UserRole() {{
            setUserId(1L);
        }});
        Assertions.assertEquals("admin", userRoles.get(0).getRole().getRoleName());
        Assertions.assertEquals("user", userRoles.get(1).getRole().getRoleName());
    }

    @Test
    void testMany(@Autowired UserMapper userMapper) {
        List<UserInfo> users = userMapper.selectList(new RowBounds(0, 5));
        Assertions.assertEquals("admin", users.get(0).getUserRoles().get(0).getRole().getRoleName());
        Assertions.assertEquals("user", users.get(0).getUserRoles().get(1).getRole().getRoleName());
    }

    @Test
    void testSelectIdUserMap() {
        Map<Long, UserInfo> idUserMap = userMapper.selectIdUserMap();
        Assertions.assertEquals(5, idUserMap.size());
    }

    @Test
    void testSelectMap() {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            val idUserMap = session.selectMap("com.ruben.mapper.UserMapper.list", "ID");
            Assertions.assertFalse(idUserMap.isEmpty());
        }
    }

    @Test
    void testSelectList() {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            val list = session.selectList("com.ruben.mapper.UserMapper.selectNames");
            Assertions.assertFalse(list.isEmpty());
        }
    }

}
